#ifndef TELESCOPE_H
#define TELESCOPE_H
#include <string>
#include <thread>
#include "SphereConv.h"
#include "stepper.h"

/*
TELESCOPE COMMANDS
------------------
Name            Syntax
----------------------
shutdown:       S
zero:           Z
//set slew rate:  R [deg/sec]
goto az/el:     G [Azimuth] [Elevation]
Track coords:   T [Azimuth] [Elevation] [Time to target]
Cancel movement:C


Maybe later:
------------
Get az/el
Get slew rate
Track earth rotation
etc.

*/

#define ACT_SHUTDOWN "S"
#define ACT_ZERO "Z"
#define ACT_SET_RATE "R"
#define ACT_GOTO_AZEL "G"
#define ACT_TRACK "T"
#define ACT_CANCEL_GOTO "C"

#define DEG_PER_SEC 20
#define DEG_PER_SEC_2 20

#define PIT_PIN 4
#define YAW_PIN 2

#define PIT_DIR 5
#define YAW_DIR 3

#define ENAB_PIN 0
#define STATUS_PIN 7

class Telescope{
private:
    int fifoFd;
    TeleAngles current;

    bool trackingToTarget;
    TeleAngles target;
    double timeToTarget;

    void zeroAxes();
    
    void goto_azel(AzimElev position);
    void goto_azel(AzimElev position, double time);
    void cancel_goto();
    
    void enableStepper(bool enable);
    
    Stepper pitchStepper;
    Stepper yawStepper;
    
    StepperMove pitMove;
    StepperMove yawMove;
    
    void moveLoop();
public:
    Telescope(int fd);
    bool doAction(std::string actionMessage);
};

#endif
