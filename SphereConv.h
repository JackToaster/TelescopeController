#ifndef SPHERE_CONV_H
#define SPHERE_CONV_H

#include <cmath>
using namespace std;
class TeleAngles {
public:
	double pitch;
	double yaw;
	TeleAngles(double pit, double y){
		pitch = pit;
		yaw = y;
	}
};


class AzimElev {
public:
	double azimuth;
	double elevation;
	AzimElev(double az, double el){
		azimuth = az;
		elevation = el;
	}
	TeleAngles getTeleAngles(){
	        const double pi = 3.14159265358979;

	        double fixedAzimuth = fmod(azimuth,360);
	        if(fixedAzimuth < -180){
	                fixedAzimuth += 360;
	        }
	        if(fixedAzimuth > 180){
	                fixedAzimuth -= 360;
	        }

	        double az = azimuth * pi / 180.0;
	        double el = elevation * pi / 180.0;

            double yaw;
            double pit;
            if(fixedAzimuth == 90){
                yaw = pi / 2.0;
                pit = el;
            } else if(fixedAzimuth == -90) {
                yaw = -pi / 2.0;
                pit = -el;
            } else {
                yaw = asin(sin(az)*cos(el));
	            pit = asin(sin(el)/cos(yaw));
            }
        	if(fixedAzimuth > 90){
        	        yaw = pi - yaw;
        	        pit *= -1.0;
        	}else if(fixedAzimuth < -90){
        	        yaw = pi - yaw;
        	        pit *= -1.0;
        	}

	        TeleAngles out(yaw * 180 / pi, pit * 180 / pi);
        	return out;
	}
};

#endif
