all: telescopeCtlD

telescopeCtlD: telescopeCtlD.o telescope.o stepper.o
	g++ telescopeCtlD.o telescope.o stepperThread.o -o telescopeCtlD -lpthread -lwiringPi

telescopeCtlD.o: telescopeCtlD.cpp
	g++ -c telescopeCtlD.cpp

telescope.o: telescope.cpp
	g++ -c telescope.cpp

stepper.o: stepper.cpp
	g++ -c stepper.cpp

clean:
	rm *.o telescopeCtlD
