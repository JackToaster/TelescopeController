#ifndef STEPPER_THREAD_H
#define STEPPER_THREAD_H
#include <chrono>
#include <queue>
#include <atomic>
#define STEP_ANGLE_DEG 1.8
#define MICRO_STEP_MODE 16

#define ANGLE_TO_STEP MICRO_STEP_MODE / STEP_ANGLE_DEG
#define STEP_HIGH_TIME_NS 100000

class StepperMove{
private:
    enum State{ACCEL, COAST, DECEL};
    State state;
    float lastStepTime;
    int decelStep;
    int halfWayStep;
    bool doAccel;
    bool doDecel;
public:
    std::chrono::steady_clock::time_point startTime;
    StepperMove(float from, float to, float maxVel, float maxAccel, float time);
    StepperMove(float from, float to, float maxVel);

    float accel;
    float vel;
    int startStep;
    int stopStep;
    bool direction;
    int current;

    float duration;

    float nextStepTime;
    float getTimeToStep();
    bool incrementStep();
};

class Stepper{
private:
    int directionPin;
    int stepPin;
    float maxAccel;
    float maxVel;
public:
    Stepper(int stepPin, int dirPin, float vel, float accel) :
        directionPin(dirPin), stepPin(stepPin), maxVel(vel), maxAccel(accel) {}

    void step(bool dir);

    bool excecuteStep(StepperMove move);

    float accelMoveTime(float from, float to);

    StepperMove move(float from, float to);
    StepperMove move(float from, float to, float time);
};


#endif
