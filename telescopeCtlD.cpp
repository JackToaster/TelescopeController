#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <syslog.h>
#include <string.h>
#include <iostream>
#include "telescope.h"

int main(void) {
        const int sleepTime = 1;
        /* Our process ID and Session ID */
        pid_t pid, sid;
        
        /* Fork off the parent process */
        pid = fork();
        if (pid < 0) {
                exit(EXIT_FAILURE);
        }
        /* If we got a good PID, then
           we can exit the parent process. */
        if (pid > 0) {
                exit(EXIT_SUCCESS);
        }

        /* Change the file mode mask */
        umask(0);
                
        /* Open any logs here */        
                
        /* Create a new SID for the child process */
        sid = setsid();
        if (sid < 0) {
                /* Log the failure */
                exit(EXIT_FAILURE);
        }
        

        
        /* Change the current working directory */
        if ((chdir("/")) < 0) {
                /* Log the failure */
                exit(EXIT_FAILURE);
        }
        
        /* Close out the standard file descriptors */
        close(STDIN_FILENO);
        close(STDOUT_FILENO);
        close(STDERR_FILENO);
        
        /* Daemon-specific initialization goes here */
        //set up fifo buffer for ipc
        //std::cout << "Started\n";
        char *fifoPath = "/tmp/telescopeCmd";
        int fd = mkfifo(fifoPath, 0666);
        //std::cout << "Created FIFO\n";
        int reader = open(fifoPath, O_RDWR);
        fcntl(reader, F_SETFL, O_NONBLOCK);
        char fifoBuf[1024];
        //std::cout << "created reader/buffer\n";
        /* setup log file to write stuff to */
        openlog("tele-daemon", LOG_PID, LOG_LOCAL1);
        //syslog(LOG_NOTICE, "Telescope initialized");
        //std::cout << "Setup and printed to syslog\n";
        /* set up telescope to send commands to */
        Telescope telescope(fd); 
        //used to split up command strings
        std::string delimiter = "\n";
       /* The Big Loop */
        while (true) {
		    sleep(sleepTime);
            //resets string input to avoid buffer overflow
            //fifoBuf[0] = '\0';
            int charsRead = read(reader, fifoBuf, 1024);
            fifoBuf[charsRead] = '\0';
            if(charsRead > 0){

                std::string stringRead(fifoBuf, charsRead);
                int pos;
                while ((pos = stringRead.find(delimiter)) != std::string::npos) {
                    std::string token = stringRead.substr(0, pos);
                    if(telescope.doAction(token)){
                        syslog(LOG_NOTICE, "Successfully excecuted command: %s", token.c_str());
                    } else {
                        syslog(LOG_WARNING, "Failed to excecute command.");
                    }
                    stringRead.erase(0, pos + delimiter.length());
                }
                
            }
            //syslog(LOG_INFO, "Tele-daemon updated");
        }
        exit(EXIT_SUCCESS);
}
