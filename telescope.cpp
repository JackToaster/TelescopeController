#include <stdio.h>
#include <unistd.h>
#include <syslog.h>
#include <string>
#include <stdexcept>
#include <vector>
#include <cmath>
#include <thread>
#include "telescope.h"
#include "stepper.h"
#include <wiringPi.h>
using namespace std;

/*
TELESCOPE COMMANDS
------------------
Name            Syntax
----------------------
shutdown:       S
zero:           Z
goto az/el:     G [Azimuth] [Elevation]
Track coords:   T [Azimuth] [Elevation] [Time to target]
Cancel movement:C


Maybe later:
------------
Get az/el
Get slew rate
Track earth rotation
etc.

*/


bool startsWith(string instring, string sub){
    if(instring.length() < sub.length())
        return false;

    for (int i=0; i<sub.length(); i++)
    {
        if (sub[i]!=instring[i])
            return false;
    }
    return true;
}



string getFirstWord(string &input){
    //syslog(LOG_INFO, "String input to getFirstWord: %s", input.c_str());
    if(input.length() <= 0)
        return "";
    const string delimiter = " ";
    int pos = input.find(delimiter);
    if(pos <= 0){
        return input;
    }
    string token = input.substr(0, pos);
    input.erase(0, pos + 1);    
    
    return token;
}

vector <double> getNumbersInCommand(string numberString, int numberCount){
    vector <double> output;
    string singleNum;
    try{
        for(int i = 0; i < numberCount; i++){
            singleNum = getFirstWord(numberString);
            output.push_back(stod(singleNum));
        }
    }catch(const invalid_argument& ex){
        syslog(LOG_WARNING, "Could not read numbers in command: %s", singleNum.c_str());
    }
    return output;
}

void enableStepper(bool enable){
    digitalWrite(STATUS_PIN, enable);
    digitalWrite(ENAB_PIN, enable);
}

void startStatusBlink(){
    digitalWrite(STATUS_PIN, false);
}

void endStatusBlink(){
    digitalWrite(STATUS_PIN, false);
}


Telescope::Telescope(int fd) : fifoFd(fd), target(0.0,0.0), current(0.0,0.0), trackingToTarget(false){
    syslog(LOG_INFO, "Telescope controller initialized");
    wiringPiSetup();
    enableStepper(true);
    zeroAxes();
    yawStepper = new Stepper(YAW_PIN, YAW_DIR, DEG_PER_SEC, DEG_PER_SEC_2);
    pitchStepper = new Stepper(PIT_PIN, PIT_DIR, DEG_PER_SEC, DEG_PER_SEC_2);
}

void Telescope::zeroAxes(){
    current = TeleAngles(0,0);
    syslog(LOG_INFO, "Zeroing telescope axes");
}

void Telescope::moveLoop(){
    while(trackingToTarget){
        bool pitMoving;
        bool yawMoving;        
        if(pitMove.getTimeToStep > yawMove.getTimeToStep){
            pitMoving = pitStepper.excecuteStep(pitMove);
        }else{
            yawMoving = yawStepper.excecuteStep(yawMove);
        }
        trackingToTarget = pitMoving || yawMoving;
    }
}

void Telescope::goto_azel(AzimElev position){
    trackingToTarget = true;
    target = position.getTeleAngles();
    target = position.getTeleAngles();
    pitMove = pitStepper.move(current.pitch, target.pitch);
    yawMove = yawStepper.move(current.yaw, target.yaw);

    current = target;

    moveLoop();
}

void Telescope::goto_azel(AzimElev position, double time){
    trackingToTarget = true;
    target = position.getTeleAngles();
    syslog(LOG_INFO, "Tracking telescope to position: Az=%f, El=%f, time=%f", position.azimuth, position.elevation, time);
    syslog(LOG_INFO, "Telescope axis angles: pit=%f, yaw=%f",target.pitch, target.yaw);
    pitMove = pitStepper.move(current.pitch, target.pitch, time);
    yawMove = yawStepper.move(current.yaw, target.yaw, time);

    current = target;

    moveLoop();
}


void Telescope::cancel_goto(){
    trackingToTarget = false;
    syslog(LOG_INFO, "Cancelling goto command\n");
}

bool Telescope::doAction(string actionMessage){
    string startWord = getFirstWord(actionMessage);
    if(startWord == ACT_ZERO){
        zeroAxes();
        return true;
    }else if(startWord == ACT_GOTO_AZEL){
        vector<double> azel = getNumbersInCommand(actionMessage, 2);
        if(azel.size() == 2){
            goto_azel(AzimElev(azel.at(0), azel.at(1)));
            return true;
        } else return false;
    }else if(startWord == ACT_CANCEL_GOTO){
        cancel_goto();
        return true;
    }else if(startWord == ACT_SHUTDOWN){
        if(trackingToTarget){
            cancel_goto();
        }
        enableStepper(false);

        syslog(LOG_NOTICE, "Shutting down telescope daemon.");        
        close(fifoFd);
        
        exit(0);
    }else if(startWord == ACT_TRACK){
        vector<double> params = getNumbersInCommand(actionMessage, 3);
        if(params.size() == 3){
            //syslog(LOG_INFO, "Params: %f %f %f", params.at(0), params.at(1), params.at(2));
            goto_azel(AzimElev(params.at(0), params.at(1)), params.at(2));
            return true;
        } else return false;
    }else {
        syslog(LOG_WARNING, "Could not recognize command in command string: %s", actionMessage.c_str());
        return false;
    }
}
