#include "SphereConv.h"
#include <cmath>
#include <iostream>
using namespace std;

int main(int argc, char **argv){
	double az;
	double el;
	cout << "Enter azimuth: ";
	cin >> az;
	cout << "Enter elevation: ";
	cin >> el;
	AzimElev input(az,el);
	TeleAngles out = input.getTeleAngles();
	cout << "Output angle: yaw=" << out.yaw << ", pit=" << out.pitch << "\n";
	return 0;
}
