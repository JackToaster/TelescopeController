#include <chrono>
#include <time.h>
#include <cmath>
#include <syslog.h>
#include "stepperThread.h"
#include <wiringPi.h>

using namespace std;

/* Stepper Move */
StepperMove::StepperMove(float from, float to, float maxVel, float maxAccel, float time){
    startTime = chrono::steady_clock::now();
    lastStepTime = 0;
    accel = maxAccel;
    vel = maxVel;
    startStep = (int)(ANGLE_TO_STEP * from);
    stopStep = (int)(ANGLE_TO_STEP * to);
    direction = from > to;
    current = startStep;
    state = ACCEL;
    doAccel = true;
    doDecel = true;
    duration = time;
}

StepperMove::StepperMove(float from, float to, float maxVel){
    startTime = chrono::steady_clock::now();
    lastStepTime = 0;
    accel = maxAccel;
    vel = maxVel;
    startStep = (int)(ANGLE_TO_STEP * from);
    stopStep = (int)(ANGLE_TO_STEP * to);
    halfWayStep =  (int)((startStep + stopStep)/2.0);
    direction = from > to;
    current = startStep;
    state = COAST;
    doAccel = false;
    doDecel = false;
}

float StepperMove::getTimeToStep(){
    if(current == stopStep){
        return 999999999;
    }
    switch(state){
        case ACCEL:
            if(accel * lastStepTime >= vel){
                state = COAST;
                decelStep = stopStep - (current - startStep);
                break;
            } else if(current >= halfWayStep){
                state = DECEL;
                break;
            }

            float a = accel;
            float b = 0;
            
            float x = solveStepTimeIntegral(a,b,lastStepTime);     
            nextStepTime = sqrt(-2.0 * a * x) / a + lastStepTime;
            break;
        case COAST:
            if(doDecel && current >= decelStep){
                state = DECEL;
            }
            nextStepTime = lastStepTime +  1.0 / vel;
            break;
        case DECEL:
            float a = -accel;
            float b = duration * accel;
            float x = solveStepTimeIntegral(a,b,lastStepTime);
            nextStepTime = (b + sqrt(b * b - 2.0 * a * x)) / a;
            break;
    }
    if(nextStepTime <= 0){
        syslog(LOG_WARN, "negative time");
        return 1;
    }
    return nextStepTime;
}

bool StepperMove:incrementStep(){
    if(current != stopStep){
        current += direction?1:-1;
        lastStepTime = nextStepTime;
        return true;
    }
    return false;
}

float solveStepTimeIntegral(float a, float b, float time){
    return (a * time * time) / 2.0 + b * time;
}

void Stepper::step(){
    digitalWrite (pinNumber, HIGH);
    struct timespec req = {0};
    req.tv_sec = STEP_HIGH_TIME_NS / 1000000000;
    req.tv_nsec = STEP_HIGH_TIME_NS % 1000000000;
    nanosleep(&req, (struct timespec *)NULL);
    digitalWrite (pinNumber, HIGH);
}

bool Stepper:excecuteStep(StepperMove move){
    if(move.incrementStep()){
        auto now = chrono::steady_clock::now();
        long currentNs = chrono::duration_cast<chrono::nanoseconds>(now - startTime).count();
        long targetNs = (long)(move.nextStepTime * 1000000000.0);
        struct timespec req = {0};
        req.tv_sec = STEP_HIGH_TIME_NS / 1000000000;
        req.tv_nsec = STEP_HIGH_TIME_NS % 1000000000;
        nanosleep(&req, (struct timespec *)NULL);
        step();
        return true;
    }
    return false;
}

float Stepper:moveTime(float from, float to){
    float absDist = abs(from - to);
    float accTime = vel / accel;
    float accDecDist = accTime * vel;
    
    if(accDecDist > absDist){
        return accTime;
    } else {
        return (absDist - accDecDist) / vel + 2.0 * accTime;
    }
}

StepperMove Stepper:move(float from, float to){
    float time = moveTime(from, to);
    return new StepperMove(from, to, maxVel, maxAccel, time);
}

StepperMove Stepper:move(float from, float to, float time){
    float absDist = abs(from - to);
    return new StepperMove(from, to, absDist / time);
}
